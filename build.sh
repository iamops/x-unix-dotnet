#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

mode=${1-Debug}
echo "mode: ${mode}"

if [ "$mode" = "Release" -o "$mode" = "Debug" ]; then
    dotnet build -c $mode ./ConsoleApp/ConsoleApp.csproj
else
    echo "mode: ${mode} is not supported"
    exit 0
fi

