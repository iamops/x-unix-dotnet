#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

# code first模式
dotnet run --project ./codefirst/CoursesAndStudents.csproj