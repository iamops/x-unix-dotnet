#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

# 
dotnet tool list --global |grep 'dotnet-ef' || {
    echo "dotnet-ef not installed, installing..."
    dotnet tool install --global dotnet-ef
}

# Northwind.db 是sqlite 数据库

#生成部分表
# dotnet ef dbcontext scaffold "Data Source=../Northwind.db" Microsoft.EntityFrameworkCore.Sqlite --table Categories --table Products --output-dir AutoGenModels --namespace WorkingWithEFCore.AutoGen --data-annotations --context NorthwindDb --project ../ConsoleApp/ConsoleApp.csproj --force

echo "从库生成dao"
dotnet ef dbcontext scaffold "Data Source=./Northwind.db" Microsoft.EntityFrameworkCore.Sqlite --output-dir AutoGen --namespace daodb.AutoGen --data-annotations --context NorthwindDb --project ./daodb/daodb.csproj --force
