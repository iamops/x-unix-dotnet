#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

dotnet --list-runtimes
dotnet --list-sdks

echo "copy db..."
cp -f ./dao/daodb/Northwind.db ./Northwind.db

echo "run ..."
dotnet run --project ./ConsoleApp/ConsoleApp.csproj