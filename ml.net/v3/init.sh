#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

dotnet new solution -n mlv3 -v diag
dotnet new console -o model
dotnet sln add model/model.csproj
cd model
dotnet add package Microsoft.ML --version 3.0.0
dotnet restore
