#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

op=${1-""}

if [ "$op" = "" ]; then
    docker cp ./inst.sh dotnet8-linux:/root/inst.sh
    docker exec -it dotnet8-linux bash -xc "chmod +x /root/inst.sh; /root/inst.sh mlnet"

elif [ "$op" = "mlnet" ]; then    
    # https://dotnet.microsoft.com/zh-cn/learn/ml-dotnet/get-started-tutorial/install
    # alpine 仓库 https://pkgs.alpinelinux.org/packages
    export BB_ASH_VERSION=""
source /etc/profile
[ -f /root/.bash_profile ] && source /root/.bash_profile
#Alpine 3.18 默认启用了新的 c-ares 库该库默认启用了 DNS over TLS，这会导致一些软件无法正常工作，比如 Docker会出现 https 无法访问的情况。
# echo "options single-request-reopen" >> /etc/resolv.conf

mlnet || {
    echo "install mlnet"
    /usr/lib/dotnet/dotnet tool install -g mlnet-linux-x64
    
cat << \EOF >> /root/.bash_profile
# 添加 .NET Core SDK 工具
export PATH="$PATH:/root/.dotnet/tools"
EOF
    source /root/.bash_profile
}

echo "进入dotnet: docker exec -it dotnet8-linux bash "
# apk add --repository=<社区存储库的URL> dotnet6-sdk
apk add dotnet6-sdk
/usr/lib/dotnet/dotnet --list-sdks

else
    echo "no args"
fi
