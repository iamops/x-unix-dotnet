# 评估指标
```
场景	评估指标	期望
数据分类	二元（准确度）/多类 (MicroAccuracy)	越接近 1.00 越好
值预测	R 平方值	越接近 1.00 越好
图像分类	精确度	越接近 1.00 越好
建议	R 平方值	越接近 1.00 越好
对象检测	精确度	越接近 1.00 越好
```
···
# ai4i2020.csv
```
这些列定义如下：

UDI：行的索引。
Product ID：包含产品类型类别和特定款式序列号的产品标识符。
Type：产品质量类别。 这些值是 L（低；所有产品的 50%）、M（中；30%）或 H（高；20%）。
Air temperature [K]、Process temperature [K]、Rotational speed [rpm]、Torque [Nm]、Tool wear [min]：从传感器收集的值。
Machine failure：二进制标签（0 或 1），用来指示机器是否出现了故障。
TWF、HDF、PWF、OSF、RNF：独立的机器故障模式。 值为 1 表示出现了相应的故障模式。

本场景不会使用数据集中的所有列，因为这些列不会为预测提供信息或包含冗余信息。
由于你希望能够预测机器是否会出现故障，因此“Machine failure”列是标签。 在 Model Builder 中，对于特征，你可以使用“Product ID”、“Type”和各种传感器列中的数据。
```

