# ml.net 3.0
+ v3

# Data
+ https://archive.ics.uci.edu/dataset/53/iris
+ https://archive.ics.uci.edu/dataset/331/sentiment+labelled+sentences 

# 官方例子
+ https://github.com/dotnet/machinelearning-samples
+ https://github.com/feiyun0112/machinelearning-samples.zh-cn

# ML.net使用方式
+ ml.net CLI
+ model builder UI 【auotML】
+ API

## Model Builder for vsstudio 2019
Model Builder, then you can download it from the following link: 
https://marketplace.visualstudio.com/items?itemName=MLNET.07
Visual Studio 2022 内置

## API
+ 鸢尾花 iris
旧版 https://www.cnblogs.com/BeanHsiang/p/9995865.html
ML.net2.0 https://www.cnblogs.com/BeanHsiang/p/12731746.html
