#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

op=${1-""}

case "$op" in
"new")
    docker exec -i dotnet6-linux bash -xc "
    cd /opt/api/iris
    dotnet new solution
    dotnet new console -o model
    dotnet sln add model/model.csproj
    cd model
    dotnet add package Microsoft.ML --version 2.0.0
    dotnet restore
    echo "data.cs Program.cs中填写完整即可训练"
"
    ;;
#容器内运行
"train")
    docker exec -i dotnet6-linux bash -xc "
    cd /opt/api/iris/
    dotnet run --project ./model/model.csproj
    echo 'model.zip outputed'
    "
    ;;
*)
# HOST机器安装dotnet6，直接可以运行
"train-local")
    dotnet run --project ./model/model.csproj
    ;;
*)
    echo "no args"
    exit 1
    ;;
esac
