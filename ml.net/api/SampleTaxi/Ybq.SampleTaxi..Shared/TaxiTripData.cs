﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.ML.Data;

namespace Ybq.SampleTaxi.Shared
{
    /// <summary>
    /// Describe the schema of the row in the training dataset.
    /// This class is going to be the input for the trained model when called in production.
    /// </summary>
    public class TaxiTripData
    {
        [LoadColumn(0)]
        public string VendorId;

        [LoadColumn(1)]
        public string RateCode;

        [LoadColumn(2)]
        public float PassengerCount;

        [LoadColumn(3)]
        public float TripTime;

        [LoadColumn(4)]
        public float TripDistance;

        [LoadColumn(5)]
        public string PaymentType;

        [LoadColumn(6)]
        public float FareAmount;
    }
}
