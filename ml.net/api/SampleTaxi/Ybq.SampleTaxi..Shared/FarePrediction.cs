﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.ML.Data;

namespace Ybq.SampleTaxi.Shared
{
    /// <summary>
    /// Describe the schema of the response given by the trained model
    /// </summary>
    public class FarePrediction
    {
        [ColumnName("Score")]
        public float FareAmount;
    }
}
