﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.ML.Data;

namespace Ybq.SampleTaxi.Shared
{
    /// <summary>
    /// Describe the schema of the response given by the TIME model
    /// </summary>
    public class TimePrediction
    {
        [ColumnName("Score")]
        public float Time;
    }
}
