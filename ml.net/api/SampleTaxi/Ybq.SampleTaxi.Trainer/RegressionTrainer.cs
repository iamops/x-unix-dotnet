﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


using System;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using Ybq.SampleTaxi.Shared;

namespace Ybq.SampleTaxi.Trainer
{
    internal class RegressionTrainer
    {
        private static string _baseModelsRelativePath = @"Output\";

        // Path(s) for the binary classifier
        private static readonly string _trainDataRelativePath = @"Data\taxi-fare-train.csv";
        private static readonly string _testDataRelativePath = @"Data\taxi-fare-test.csv";
        private static readonly string _binaryTrainDataPath = DebugHelpers.GetAbsolutePath(_trainDataRelativePath);
        private static readonly string _binaryTestDataPath = DebugHelpers.GetAbsolutePath(_testDataRelativePath);
        private static readonly string _modelFareRelativePath = $"{_baseModelsRelativePath}\\SampleTaxi.Fare.zip";
        private static readonly string _modelFarePath = DebugHelpers.GetAbsolutePath(_modelFareRelativePath);
        private static readonly string _modelTimeRelativePath = $"{_baseModelsRelativePath}\\SampleTaxi.Time.zip";
        private static readonly string _modelTimePath = DebugHelpers.GetAbsolutePath(_modelTimeRelativePath);


        public static void Execute()
        {
            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 1: Startup
            var mlContext = new MLContext();
            Console.WriteLine("Loading data for the linear regressor...");
            
            // Load data to be used for training/testing
            IDataView dataViewTraining = mlContext.Data.LoadFromTextFile<TaxiTripData>(
                _binaryTrainDataPath, hasHeader: true, separatorChar: ',');
            IDataView dataViewTesting = mlContext.Data.LoadFromTextFile<TaxiTripData>(
                _binaryTestDataPath, hasHeader: true, separatorChar: ',');

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 2: Prepare the dataset for FARE model training
            Console.WriteLine("Preparing data...");
            dataViewTraining = RemoveOutliers(mlContext, dataViewTraining);
            var fareDataProcessing = ComposeFareDataProcessingPipeline(mlContext);  
            DebugHelpers.ShowTransformedData(mlContext, dataViewTraining, fareDataProcessing);

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 3: Train, evaluate and save the FARE model 
            Console.WriteLine("Training the model (fare) ...");
            TrainEvaluateSaveModel(mlContext, dataViewTraining, dataViewTesting, fareDataProcessing, _modelFarePath);

            

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 2: Prepare the dataset for TIME model training
            Console.WriteLine("Preparing data...");
            dataViewTraining = RemoveOutliers(mlContext, dataViewTraining);
            var timeDataProcessing = ComposeTimeDataProcessingPipeline(mlContext);  
            DebugHelpers.ShowTransformedData(mlContext, dataViewTraining, timeDataProcessing);

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 3: Train, evaluate and save the FARE model 
            Console.WriteLine("Training the model (time) ...");
            TrainEvaluateSaveModel(mlContext, dataViewTraining, dataViewTesting, timeDataProcessing, _modelTimePath);
        }


        #region PRIVATE

        /// <summary>
        /// Remove outliers for fares higher than $150 and lower than $1
        /// </summary>
        /// <param name="context"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private static IDataView RemoveOutliers(MLContext context, IDataView data)
        {
            var modifiedDataView = context.Data.FilterRowsByColumn(data, 
                "FareAmount", 
                lowerBound: 1, 
                upperBound: 150);
            return modifiedDataView;
        }

        /// <summary>
        /// Feature engineering for the fare model
        /// </summary>
        /// <param name="context">ML library context</param>
        /// <returns></returns>
        private static IEstimator<ITransformer> ComposeFareDataProcessingPipeline(MLContext context)
        {
            var pipeline = context.Transforms.CopyColumns("Label", "FareAmount")
                .Append(context.Transforms.Categorical.OneHotEncoding("VendorIdEncoded", "VendorId")) 
                .Append(context.Transforms.Categorical.OneHotEncoding("RateCodeEncoded", "RateCode"))
                .Append(context.Transforms.Categorical.OneHotEncoding("PaymentTypeEncoded", "PaymentType"))
                .Append(context.Transforms.NormalizeMeanVariance("PassengerCount"))
                .Append(context.Transforms.NormalizeMeanVariance("TripTime"))
                .Append(context.Transforms.NormalizeMeanVariance("TripDistance"))
                .Append(context.Transforms.Concatenate("Features",
                    "VendorIdEncoded", 
                    "RateCodeEncoded", 
                    "PaymentTypeEncoded", 
                    "PassengerCount", 
                    "TripTime", 
                    "TripDistance"));
            return pipeline;
        }

        /// <summary>
        /// Feature engineering for the time model
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static IEstimator<ITransformer> ComposeTimeDataProcessingPipeline(MLContext context)
        {
            var pipeline = context.Transforms.CopyColumns("Label", "TripTime")
                .Append(context.Transforms.Categorical.OneHotEncoding("VendorIdEncoded", "VendorId")) 
                .Append(context.Transforms.Categorical.OneHotEncoding("RateCodeEncoded", "RateCode"))
                .Append(context.Transforms.Categorical.OneHotEncoding("PaymentTypeEncoded", "PaymentType"))
                .Append(context.Transforms.NormalizeMeanVariance("PassengerCount"))
                .Append(context.Transforms.NormalizeMeanVariance("TripDistance"))
                .Append(context.Transforms.Concatenate("Features",
                    "VendorIdEncoded", 
                    "RateCodeEncoded", 
                    "PaymentTypeEncoded", 
                    "PassengerCount", 
                    "TripDistance"));

            // A pipeline is always a chain of estimators even when it is only made by data transfomers as in this case.
            // Hence you can fit this pipeline on data and train transformers. When do you need it? When you have 
            // transfomers that are not simply value converters (ie, Conversion.ConvertType) or column managers but need to look 
            // at the entire dataset (ie, one-hot encoding, normalizers)
            return pipeline;
        }

        /// <summary>
        /// Train the model and save the resulting file
        /// </summary>
        /// <param name="mlContext">ML Context</param>
        /// <param name="trainingDataView">DataView for training</param>
        /// <param name="testDataView">DataView for testing</param>
        /// <param name="dataProcessPipeline">Processing pipeline for data</param>
        /// <param name="modelPath">Where to save the model</param>
        /// <param name="useCrossValidation">Validation type</param>
        /// <returns></returns>
        private static void TrainEvaluateSaveModel(MLContext mlContext, 
            IDataView trainingDataView, 
            IDataView testDataView, 
            IEstimator<ITransformer> dataProcessPipeline, 
            string modelPath,
            bool useCrossValidation = false)
        {
            if (useCrossValidation) // Use K-FOLD 
            {
                // Set the training algorithm          
                var trainer = mlContext
                    .Regression
                    .Trainers
                    .OnlineGradientDescent("Label", "Features", lossFunction: new SquaredLoss());

                // Create data prep transformer and actually trasform data
                ITransformer dataPrepTransformer = dataProcessPipeline.Fit(trainingDataView);
                IDataView transformedData = dataPrepTransformer.Transform(trainingDataView);

                // Train models with cross validation and select the best one
                var cvResults = mlContext.Regression.CrossValidate(transformedData, trainer, numberOfFolds: 5);
                ITransformer[] models =
                    cvResults
                        .OrderByDescending(fold => fold.Metrics.RSquared)
                        .Select(fold => fold.Model)
                        .ToArray();
                var cvMetrics = cvResults
                    .OrderByDescending(fold => fold.Metrics.RSquared)
                    .Select(fold => fold.Metrics)
                    .ToArray();

                // Get best model
                ITransformer trainedModel = models[0];
                RegressionMetrics metrics = cvMetrics[0];

                ConsoleHelper.PrintRegressionMetrics(trainer.ToString(), metrics);

                mlContext.Model.Save(trainedModel, trainingDataView.Schema, modelPath);
                Console.WriteLine("The model is saved to {0}\n\n", modelPath);
            }
            else // HOLDOUT Version
            {
                // Set the training algorithm                  
                var trainer = mlContext
                    .Regression
                    .Trainers
                    .Sdca("Label", "Features", lossFunction: new SquaredLoss());
                var trainingPipeline = dataProcessPipeline.Append(trainer);

                // Train the model fitting to the training dataset
                var trainedModel = trainingPipeline.Fit(trainingDataView);

                // Evaluate the model and show accuracy stats
                IDataView predictions = trainedModel.Transform(testDataView); 

                var metrics = mlContext.Regression.Evaluate(predictions, labelColumnName: "Label", scoreColumnName: "Score");
                ConsoleHelper.PrintRegressionMetrics(trainer.ToString(), metrics);

                // Save the trained model to a .ZIP file
                mlContext.Model.Save(trainedModel, trainingDataView.Schema, modelPath);

                Console.WriteLine("The model is saved to {0}\n\n", modelPath);
            }
        }
        #endregion
    }
}
