﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


using System.IO;
using Microsoft.ML;

namespace Ybq.SampleTaxi.Trainer
{
    class DebugHelpers
    {
        /// <summary>
        /// Append current path to a relative path
        /// </summary>
        /// <param name="relativePath">Relative path to complete</param>
        /// <returns>Absoltue path</returns>
        public static string GetAbsolutePath(string relativePath)
        {
            var assemblyFolderPath = new FileInfo(typeof(Program).Assembly.Location).DirectoryName;
            if (assemblyFolderPath == null)
                return "";

            var basePath = Path.GetFullPath(Path.Combine(assemblyFolderPath, @"..\..\..\"));
            return Path.Combine(basePath, relativePath);
        }

        /// <summary>
        /// Data preview
        /// </summary>
        /// <param name="mlContext"></param>
        /// <param name="dataView"></param>
        /// <param name="pipeline"></param>
        public static void ShowTransformedData(MLContext mlContext, IDataView dataView, IEstimator<ITransformer> pipeline)
        {
            // Peek data after applying the pipeline's transformations
            ConsoleHelper.PeekDataViewInConsole(mlContext, dataView, pipeline, 5 /* number of rows to preview */);
        }
    }
}
