﻿//////////////////////////////////////////////////////////////
//
//  PROGRAMMING ASP.NET CORE
//  Youbiquitous 2019
//  Starter Kit
//  Dino Esposito
//


namespace Youbiquitous.TaxiFair.Shared
{
    public class CommandResponse
    {
        public static CommandResponse Ok = new CommandResponse(true);
        public static CommandResponse Fail = new CommandResponse(false);

        public CommandResponse(bool success = false)
        {
            Success = success;
        }

        public bool Success { get; }
        public string Message { get; private set; }
        public string RedirectUrl { get; private set; }
        public string Key { get; private set; }

        public CommandResponse AddMessage(string message)
        {
            Message = message;
            return this;
        }

        public CommandResponse AddRedirectUrl(string url)
        {
            RedirectUrl = url;
            return this;
        }

        public CommandResponse AddKey(string key)
        {
            Key = key;
            return this;
        }
    }
}