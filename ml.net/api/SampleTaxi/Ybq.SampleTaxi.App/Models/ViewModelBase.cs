﻿//////////////////////////////////////////////////////////////
//
//  PROGRAMMING ASP.NET CORE
//  Youbiquitous 2019
//  Starter Kit L1
//  Dino Esposito
//

using Expoware.Youbiquitous.Core.Extensions;
using Youbiquitous.TaxiFair.Common.Settings;

namespace Youbiquitous.TaxiFair.Models
{
    public class ViewModelBase
    {
        protected ViewModelBase(string title = "")
        {
            Settings = GlobalAppSettings.Instance;
            if (title.IsNullOrWhitespace())
                title = Settings.General.ApplicationTitle;

            Title = title;
        }

        public static ViewModelBase Default(string title = "")
        {
            var model = new ViewModelBase(title);
            return model;
        }

        public string Title { get; set; }

        public GlobalAppSettings Settings { get; }

        public virtual bool IsValid()
        {
            return true;
        }
    }
}