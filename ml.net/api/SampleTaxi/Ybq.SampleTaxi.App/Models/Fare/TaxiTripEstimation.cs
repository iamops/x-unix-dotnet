﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using System;

namespace Ybq.SampleTaxi.App.Models.Fare
{
    public class TaxiTripEstimation
    {
        public int NumberOfPassengers { get; set; }
        public int CarType { get; set; }
        public string PaymentType { get; set; }
        public float Distance { get; set; }

        public float EstimatedFare { get; set; }
        public float EstimatedTime { get; set; }
        public string EstimatedFareForDisplay { get; set; }
        public string EstimatedTimeForDisplay { get; set; }

        public static string FareForDisplay(float fare)
        {
            var rounded = Math.Round(fare);
            var range1 = rounded - (rounded * 0.2);
            var range2 = rounded + (rounded * 0.2);
            return $"{Math.Round(range1)} - {Math.Round(range2)}";
        }

        public static string TimeForDisplay(float time)
        {
            return $"{Math.Round(time/60)} min";
        }
    }
}