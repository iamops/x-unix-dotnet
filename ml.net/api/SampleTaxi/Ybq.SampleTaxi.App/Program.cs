﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Youbiquitous.TaxiFair;

namespace Ybq.SampleTaxi.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
