﻿///////////////////////////////////////////////////////////////////
//
// Youbiquitous YBQ : app starter 
// Copyright (c) Youbiquitous srl 2019
//
// Author: Dino Esposito (http://youbiquitous.net)
//

(function () {
    Ybq.configureCommonElements();
    Ybq.mobilize();
})();