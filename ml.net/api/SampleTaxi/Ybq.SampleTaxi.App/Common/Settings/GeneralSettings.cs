﻿//////////////////////////////////////////////////////////////
//
//  PROGRAMMING ASP.NET CORE
//  Youbiquitous 2019
//  Starter Kit
//  Dino Esposito
//

namespace Youbiquitous.TaxiFair.Common.Settings
{
    public class GeneralSettings
    {
        public string ApplicationTitle { get; set; }
        public string Copyright { get; set; }
        public string Version { get; set; }
        public string ReleaseNote { get; set; }
    }
}