﻿//////////////////////////////////////////////////////////////
//
//  PROGRAMMING ASP.NET CORE
//  Youbiquitous 2019
//  Starter Kit
//  Dino Esposito
//



using System.Collections.Generic;
using System.Globalization;
using Expoware.Youbiquitous.Core.Extensions;

namespace Youbiquitous.TaxiFair.Common.Settings
{
    public class GlobalAppSettings
    {
        public static GlobalAppSettings Instance = new GlobalAppSettings();

        public GlobalAppSettings()
        {
            Languages = new List<string>();
            General = new GeneralSettings();
            Run = new RunSettings();
            Instance = this;
        }

        public List<string> Languages { get; }
        public GeneralSettings General { get; set; }
        public RunSettings Run { get; set; }

        public IList<CultureInfo> SupportedCultures()
        {
            var list = new List<CultureInfo>();
            foreach (var language in Languages)
            {
                if (language.IsNullOrWhitespace())
                    continue;
                list.Add(new CultureInfo(language));
            }
            return list;
        }

        public static IList<CultureInfo> GetSupportedCultures(string[] languages)
        {
            if (languages == null)
                return new List<CultureInfo>();

            var list = new List<CultureInfo>();
            foreach (var language in languages)
            {
                if (language.IsNullOrWhitespace())
                    continue;
                list.Add(new CultureInfo(language));
            }

            return list;
        }
    }
}