﻿//////////////////////////////////////////////////////////////
//
//  PROGRAMMING ASP.NET CORE
//  Youbiquitous 2019
//  Starter Kit
//  Dino Esposito
//


using System.Collections.Generic;

namespace Youbiquitous.TaxiFair.Common.Settings
{
    public class RunSettings
    {
        public RunSettings()
        {
            DevMode = true;
            EnableLogging = false;
            EnableDevExceptions = false;
        }

        public bool DevMode { get; set; }
        public bool EnableLogging { get; set; }
        public bool EnableDevExceptions { get; set; }
    }
}