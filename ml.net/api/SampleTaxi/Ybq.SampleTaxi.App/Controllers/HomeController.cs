﻿//////////////////////////////////////////////////////////////
//
//  PROGRAMMING ASP.NET CORE
//  Youbiquitous 2019
//  Starter Kit
//  Dino Esposito
//

using Microsoft.AspNetCore.Mvc;
using Youbiquitous.TaxiFair.Models;

namespace Youbiquitous.TaxiFair.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = ViewModelBase.Default();
            model.Title = model.Settings.General.ApplicationTitle;
            return View(model);
        }

        public IActionResult Test()
        {
            return Content("BOOH");
        }
    }
}
