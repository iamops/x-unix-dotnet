﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.ML;
using Ybq.SampleTaxi.App.Application;
using Ybq.SampleTaxi.App.Models.Fare;
using Ybq.SampleTaxi.Shared;

namespace Ybq.SampleTaxi.App.Controllers
{
    /// <summary>
    /// Wrapper for the engine(s)
    /// </summary>
    public class FareController : Controller
    {
        private readonly FarePredictionService _service;

        public FareController(PredictionEnginePool<TaxiTripData, FarePrediction> fare,
            PredictionEnginePool<TaxiTripData, TimePrediction> time)
        {
            _service = new FarePredictionService(fare, time);
        }

        /// <summary>
        /// Endpoint invoked from the UI to get a response from the ML model
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public IActionResult Suggest(TaxiTripEstimation input)
        {
            var response = _service.Predict(input);
            return Json(response);
        }
    }
}
