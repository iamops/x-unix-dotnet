﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.Extensions.ML;
using Microsoft.ML;
using Ybq.SampleTaxi.App.Models.Fare;
using Ybq.SampleTaxi.Shared;

namespace Ybq.SampleTaxi.App.Application
{
    public class FarePredictionService
    {
        private readonly PredictionEnginePool<TaxiTripData, FarePrediction> _engineFare;
        private readonly PredictionEnginePool<TaxiTripData, TimePrediction> _engineTime;

        public FarePredictionService(PredictionEnginePool<TaxiTripData, FarePrediction> engineFare,
            PredictionEnginePool<TaxiTripData, TimePrediction> engineTime)
        {
            _engineFare = engineFare;
            _engineTime = engineTime;
        }

        /// <summary>
        /// Invoker of the ML model
        /// </summary>
        /// <param name="input">Data coming the HTML view to challenge the model</param>
        /// <returns></returns>
        public TaxiTripEstimation Predict(TaxiTripEstimation input)
        {
            var trip = new TaxiTripData()
            {
                VendorId = "VTS",
                RateCode = input.CarType.ToString(),
                PassengerCount = input.NumberOfPassengers,
                PaymentType = input.PaymentType,
                TripDistance = input.Distance,

                // To predict
                FareAmount = 0,
                TripTime = 0
            };

            // Predict amount and time
            trip.FareAmount = _engineFare.Predict("SampleTaxi.Fare", trip).FareAmount;  
            trip.TripTime = _engineTime.Predict("SampleTaxi.Time", trip).Time;  

            // Convert
            input.EstimatedFare = trip.FareAmount;
            input.EstimatedTime = trip.TripTime;
            input.EstimatedFareForDisplay = TaxiTripEstimation.FareForDisplay(trip.FareAmount);
            input.EstimatedTimeForDisplay = TaxiTripEstimation.TimeForDisplay(trip.TripTime);
            return input;
        }

        private static float TestTimePrediction(TaxiTripData trip, MLContext mlContext, string modelPath)
        {
            // Load saved trained model
            ITransformer trainedModel = mlContext.Model.Load(modelPath, out var modelInputSchema);

            // Create prediction engine related to the loaded trained model
            var predEngine = mlContext
                .Model
                .CreatePredictionEngine<TaxiTripData, TimePrediction>(trainedModel);

            // Predict
            var prediction = predEngine.Predict(trip);
            return prediction.Time;
        }
    }
}