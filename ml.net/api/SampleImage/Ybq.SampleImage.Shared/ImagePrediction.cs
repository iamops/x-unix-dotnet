﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

namespace Ybq.SampleImage.Shared
{
    public class ImagePrediction : ImageData
    {
        public float[] Score;
        public string PredictedLabelValue;
    }
}
