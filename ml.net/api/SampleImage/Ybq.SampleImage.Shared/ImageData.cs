﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.ML.Data;

namespace Ybq.SampleImage.Shared
{
    /// <summary>
    /// Describes our dataset (to be used on top of the Inception Model)
    /// </summary>
    public class ImageData
    {
        [LoadColumn(0)]
        public string ImagePath;

        [LoadColumn(1)]
        public string Label;
    }
}
