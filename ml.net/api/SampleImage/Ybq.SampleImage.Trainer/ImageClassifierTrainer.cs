﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


using System;
using Microsoft.ML;
using Microsoft.ML.Transforms.Image;
using Ybq.SampleImage.Shared;

namespace Ybq.SampleImage.Trainer
{
    internal class ImageClassifierTrainer
    {
        private static string _baseModelsRelativePath = @"Output\";

        // Path(s) for the image classifier
        private static readonly string _trainTagsRelativePath = DebugHelpers.GetAbsolutePath(@"Data\CustomImages\tags.tsv");
        private static readonly string _trainImagesRelativePath = DebugHelpers.GetAbsolutePath(@"Data\CustomImages");
        private static readonly string _inceptionPb = DebugHelpers.GetAbsolutePath(@"Data\Inception\tensorflow_inception_graph.pb");
        private static readonly string _modelRelativePath = $"{_baseModelsRelativePath}\\SampleImage.Composition.zip";
        private static readonly string _modelPath = DebugHelpers.GetAbsolutePath(_modelRelativePath);


        public static void Execute()
        {
            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 1: Startup
            var mlContext = new MLContext();
            Console.WriteLine("Loading data for the image classifier (composed transfer learning)...");
            
            // Load custom tags to be used for training
            IDataView dataViewTraining = mlContext.Data.LoadFromTextFile<ImageData>(_trainTagsRelativePath);

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 2: Building the data pipeline
            var converter1 = mlContext.Transforms.Conversion.MapValueToKey("LabelKey", "Label");
            var loading = mlContext
                .Transforms
                .LoadImages("input", _trainImagesRelativePath, "ImagePath");
            var resizing = mlContext
                .Transforms
                .ResizeImages("input", 
                    InceptionSettings.ImageWidth, 
                    InceptionSettings.ImageHeight,
                    "input");
            var extracting = mlContext
                .Transforms
                .ExtractPixels("input",
                    null,
                    ImagePixelExtractingEstimator.ColorBits.Rgb,
                    ImagePixelExtractingEstimator.ColorsOrder.ARGB,
                    InceptionSettings.ChannelsLast,
                    InceptionSettings.Mean);

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 3: Connecting to the Inception pre-trained model
            var inceptionPipeline = mlContext
                .Model
                .LoadTensorFlowModel(_inceptionPb)
                .ScoreTensorFlowModel(new[] { "softmax2_pre_activation" }, new[] { "input" }, addBatchDimensionInput: true);

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 4: Add the multiclass classification pieces
            var trainer = mlContext
                .MulticlassClassification
                .Trainers
                .LbfgsMaximumEntropy("LabelKey", "softmax2_pre_activation");
            var converter2 = mlContext
                .Transforms
                .Conversion
                .MapKeyToValue("PredictedLabelValue", "PredictedLabel");

            // Final training pipeline
            var trainingPipeline = converter1
                .Append(loading)
                .Append(resizing)
                .Append(extracting)
                .Append(inceptionPipeline)
                .Append(trainer)
                .Append(converter2)
                .AppendCacheCheckpoint(mlContext);

            /////////////////////////////////////////////////////////////////////////////////////
            // STEP 5: Train and save the model
            ITransformer model = trainingPipeline.Fit(dataViewTraining);
            mlContext.Model.Save(model, dataViewTraining.Schema, _modelPath);

            //var classifierOptions = new ImageClassificationTrainer.Options()
            //{
            //    FeatureColumnName = "Image",
            //    LabelColumnName = "LabelAsKey",
            //    Arch = ImageClassificationTrainer.Architecture.InceptionV3,
            //    ReuseTrainSetBottleneckCachedValues = true,
            //    ReuseValidationSetBottleneckCachedValues = true
            //};
            //var pipeline = mlContext
            //    .MulticlassClassification
            //    .Trainers
            //    .ImageClassification(classifierOptions)
            //    .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));
        }
    }
}
