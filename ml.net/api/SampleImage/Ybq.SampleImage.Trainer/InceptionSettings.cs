﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

namespace Ybq.SampleImage.Trainer
{
    public class InceptionSettings
    {
        public const int ImageHeight = 224;
        public const int ImageWidth = 224;
        public const float Mean = 117;
        public const float Scale = 1;
        public const bool ChannelsLast = true;
    }
}