﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


using System;
using System.IO;

namespace Ybq.SampleImage.Trainer
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Welcome to Programming ML.NET!\n");
            EnsureOutputDirectory();

            // Regression 
            ImageClassifierTrainer.Execute();

            Console.WriteLine("\n\nDONE. Press any key.");
            Console.ReadKey();
        }


        /// <summary>
        /// Ensure the output directory where the model will be saved exists
        /// </summary>
        private static void EnsureOutputDirectory()
        {
            var assemblyFolderPath = new FileInfo(typeof(Program).Assembly.Location).DirectoryName;
            if (assemblyFolderPath == null)
                return;

            var dir = Path.GetFullPath(Path.Combine(assemblyFolderPath, @"..\..\..\Output"));
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
    }
}