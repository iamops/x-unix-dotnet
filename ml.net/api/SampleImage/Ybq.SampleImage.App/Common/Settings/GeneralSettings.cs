﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

namespace Ybq.SampleImage.App.Common.Settings
{
    public class GeneralSettings
    {
        public string ApplicationTitle { get; set; }
        public string Copyright { get; set; }
        public string Version { get; set; }
        public string ReleaseNote { get; set; }
    }
}