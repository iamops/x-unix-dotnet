﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


namespace Ybq.SampleImage.App.Common.Settings
{
    public class RunSettings
    {
        public RunSettings()
        {
            DevMode = true;
            EnableLogging = false;
            EnableDevExceptions = false;
        }

        public bool DevMode { get; set; }
        public bool EnableLogging { get; set; }
        public bool EnableDevExceptions { get; set; }
    }
}