﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


using System;
using System.Collections.Generic;

namespace Ybq.SampleImage.App.Common.Exceptions
{
    public class YbqSampleException : Exception
    {
        public YbqSampleException()
        {
            RecoveryLinks = new List<RecoveryLink>();
        }

        public YbqSampleException(string message) : base(message)
        {
            RecoveryLinks = new List<RecoveryLink>();
        }

        public YbqSampleException(Exception exception) : this(exception.Message)
        {
        }

        public List<RecoveryLink> RecoveryLinks { get; }

        public YbqSampleException AddRecoveryLink(string text, string url)
        {
            RecoveryLinks.Add(new RecoveryLink(text, url));
            return this;
        }

        public YbqSampleException AddRecoveryLink(RecoveryLink link)
        {
            RecoveryLinks.Add(link);
            return this;
        }
    }

    public class RecoveryLink
    {
        public RecoveryLink(string text, string url)
        {
            Text = text;
            Url = url;
        }
        public string Text { get; }
        public string Url { get; }
    }

}