﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.ML;
using Ybq.SampleImage.App.Common.Settings;
using Ybq.SampleImage.Shared;

namespace Ybq.SampleImage.App
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var dom = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
            Configuration = dom;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<GlobalAppSettings>(Configuration);

            // Create a pool of prediction builders
            var mlModelPath = Path.Combine(Environment.ContentRootPath, @"ml\SampleImage.Composition.zip");

            services.AddPredictionEnginePool<ImageData, ImagePrediction>()
                .FromFile(modelName: "SampleImage.Composition", filePath: mlModelPath);

            // Configure for ASP.NET MVC
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
