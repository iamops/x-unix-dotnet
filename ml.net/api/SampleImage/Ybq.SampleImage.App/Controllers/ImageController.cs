﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.ML;
using Ybq.SampleImage.App.Application;
using Ybq.SampleImage.App.Models.Image;
using Ybq.SampleImage.Shared;

namespace Ybq.SampleImage.App.Controllers
{
    /// <summary>
    /// Wrapper for the engine(s)
    /// </summary>
    public class ImageController : Controller
    {
        private readonly ImageService _service;
        private readonly IWebHostEnvironment _env;

        public ImageController(PredictionEnginePool<ImageData, ImagePrediction> imgClassifierEngine, IWebHostEnvironment env)
        {
            _service = new ImageService(imgClassifierEngine);
            _env = env;
        }

        /// <summary>
        /// Endpoint invoked from the UI to get a response from the ML model
        /// </summary>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        public async Task<IActionResult> Suggest(IFormFile imageFile)
        {
            // Save the image
            if (imageFile == null)
                return null;
            
            var filePath = $"{_env.WebRootPath}\\uploads\\{imageFile.FileName}";
            await using var fs = System.IO.File.Create(filePath);
            await imageFile.CopyToAsync(fs);
            fs.Close();

            var input = new ClassifiedImage {ImageFile = filePath};
            var response = _service.Predict(input);
            return Json(response);
        }
    }
}
