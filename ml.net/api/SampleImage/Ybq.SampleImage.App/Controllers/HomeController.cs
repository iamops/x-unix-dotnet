﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using Microsoft.AspNetCore.Mvc;
using Ybq.SampleImage.App.Models;

namespace Ybq.SampleImage.App.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = ViewModelBase.Default();
            model.Title = model.Settings.General.ApplicationTitle;
            return View(model);
        }

        public IActionResult Test()
        {
            return Content("BOOH");
        }
    }
}
