﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using System;
using System.IO;
using Microsoft.Extensions.ML;
using Ybq.SampleImage.App.Models.Image;
using Ybq.SampleImage.Shared;

namespace Ybq.SampleImage.App.Application
{
    public class ImageService
    {
        private readonly PredictionEnginePool<ImageData, ImagePrediction> _engineImageClassifier;

        public ImageService(PredictionEnginePool<ImageData, ImagePrediction> engineImageClassifier)
        {
            _engineImageClassifier = engineImageClassifier;
        }

        /// <summary>
        /// Invoker of the ML model
        /// </summary>
        /// <param name="input">Data coming the HTML view to challenge the model</param>
        /// <returns></returns>
        public ClassifiedImage Predict(ClassifiedImage input)
        {
            var img = new ImageData
            {
                ImagePath = input.ImageFile
            };
             
            // Predict amount and time
            try
            {
                var prediction = _engineImageClassifier.Predict("SampleImage.Composition", img);

                input.TargetClass = prediction.PredictedLabelValue;
                input.Score = prediction.Score;
                input.ImageUrl = $"uploads/{Path.GetFileName(input.ImageFile)}";
                return input;
            }
            catch (Exception ex)
            {
                var x = 0;
                throw;
            }
           
        }
    }
}