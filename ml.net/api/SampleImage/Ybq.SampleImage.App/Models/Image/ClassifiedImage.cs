﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//

using System;

namespace Ybq.SampleImage.App.Models.Image
{
    public class ClassifiedImage
    {
        public string ImageFile { get; set; }
        public string TargetClass { get; set; }
        public float[] Score { get; set; }
        public string ImageUrl { get; set; }
    }
}