﻿///////////////////////////////////////////////////////////////////
//
//  Programming ML.NET
//  End-to-end example
//
//  (c) Youbiquitous srl 2021
//  https://youbiquitous.net
//


using Expoware.Youbiquitous.Core.Extensions;
using Ybq.SampleImage.App.Common.Settings;

namespace Ybq.SampleImage.App.Models
{
    public class ViewModelBase
    {
        protected ViewModelBase(string title = "")
        {
            Settings = GlobalAppSettings.Instance;
            if (title.IsNullOrWhitespace())
                title = Settings.General.ApplicationTitle;

            Title = title;
        }

        public static ViewModelBase Default(string title = "")
        {
            var model = new ViewModelBase(title);
            return model;
        }

        public string Title { get; set; }

        public GlobalAppSettings Settings { get; }

        public virtual bool IsValid()
        {
            return true;
        }
    }
}