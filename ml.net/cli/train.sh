#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

#  UCI 机器学习存储库中的带情绪标签的句子数据集。
#   https://archive.ics.uci.edu/dataset/331/sentiment+labelled+sentences
:<<EOF
mlnet classification 命令使用 AutoML 运行 ML.NET，在给定的训练时间内，通过数据转换、算法和算法选项的不同组合，探索分类模型的多次迭代，然后选择性能最高的模型。

--dataset: 你选择了 yelp_labelled.txt 作为数据集(在内部，CLI 会将一个数据集拆分为定型数据集和测试数据集)。
--label-col: 必须指定要预测的目标列(或标签)。这种情况下，需要预测第二列中的情绪(零索引列代表这是列 "1")。
--has-header: 使用此选项指定数据集是否具有标头。在这种情况下，数据集没有标头，因此它是 false。
--name: 使用此选项可为机器学习模型和相关资产提供名称。在这种情况下，与此机器学习模型关联的所有资产的名称都具有 SentimentModel。
--train-time: 还必须指定希望 ML.NET CLI 浏览不同模型的时间量。在这种情况下为 60 秒(如果训练后未找到模型，则可以尝试增加此数字)。请注意，对于较大的数据集，应设置更长的训练时间。
EOF

docker exec -it dotnet6-linux bash -c "echo trains; cd /opt/; 
mlnet classification --dataset "./datas/uci/yelp_labelled.txt" --label-col 1 --has-header false --name SentimentModel  --train-time 60"

:<<EOF
已创建名为 SentimentModel 的新目录，它包含 .NET 控制台应用，此应用包括以下文件:
Program.cs: 此文件包含用于运行模型的代码。
SentimentModel.consumption.cs: 此文件包含模型输入和输出类以及可用于模型消耗的 Predict 方法。
SentimentModel.mbconfig: 此文件是一个 JSON 文件，用于跟踪训练中的配置和结果。
SentimentModel.training.cs: 此文件包含用于训练最终模型的训练管道(数据转换、算法和算法参数)。
SentimentModel.zip: 该文件是经过训练的 ML.NET 模型，它是一个序列化的 zip 文件。
EOF