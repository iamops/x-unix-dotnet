#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

op=${1-""}

# ml.net cli训练出模型，这里直接使用
if [ "$op" = "" ]; then
    docker exec -it dotnet6-linux bash -c "echo UseModel; cd /opt/cli/SentimentModel; 
    dotnet run
    "
else
    echo "run local"
    cd SentimentModel
    dotnet run
fi