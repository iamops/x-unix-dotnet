#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

v=${1-6}
echo "dotnet version: $v"

[ -f ./Dockerfile.${v} ] ||{
    echo "Dockerfile.${v}不存在"
    exit 1
}

echo "check sdk file..."
[ -f ./pkg/dotnet-sdk-6.0.417-linux-musl-x64.tar.gz ] ||{
    curl -o ./pkg/dotnet-sdk-6.0.417-linux-musl-x64.tar.gz "https://dotnet.microsoft.com/zh-cn/download/dotnet/thank-you/sdk-6.0.417-linux-x64-alpine-binaries"
}
[ -f ./pkg/dotnet-sdk-8.0.100-linux-musl-x64.tar.gz ] ||{
    curl -o ./pkg/dotnet-sdk-8.0.100-linux-musl-x64.tar.gz "https://dotnet.microsoft.com/zh-cn/download/dotnet/thank-you/sdk-8.0.100-linux-x64-alpine-binaries"
}

echo "build donet images..."
docker build -t dot${v} -f ./Dockerfile.${v} .

echo "done"