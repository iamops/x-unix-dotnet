#!/bin/bash
set -euo pipefail
BaseDir=$(cd "$(dirname "$0")"; pwd)
cd ${BaseDir}

docker build -t alpine-dotnet8 .

docker run -it --rm alpine-dotnet8 bash -c "source /etc/profile; env; dotnet --version"

echo "docker run -it --rm alpine-dotnet8"
