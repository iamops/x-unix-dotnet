# CLI
dotnet new globaljson --sdk-version 8.0.100

dotnet publish -c Release -r --self-contained .
dotnet publish -c Release -r linux-x64 --self-contained .

dotnet publish -c Release -r linux-arm64 --self-contained

# ef core
https://learn.microsoft.com/zh-cn/ef/core/cli/dotnet?view=azure-ef
dotnet tool install --global dotnet-ef
dotnet ef
